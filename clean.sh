#!/bin/bash
# this can be called from the shell e.g. before git

if [ -d build ]; then
   echo "@@@ removing build dir @@@"
   rm -rf build
fi

tree -a